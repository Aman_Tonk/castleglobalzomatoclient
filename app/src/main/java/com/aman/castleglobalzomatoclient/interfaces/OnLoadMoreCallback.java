package com.aman.castleglobalzomatoclient.interfaces;

/**
 * Created by Aman on 27-11-2017.
 */

public interface OnLoadMoreCallback {
    void onLoadMore();
}
