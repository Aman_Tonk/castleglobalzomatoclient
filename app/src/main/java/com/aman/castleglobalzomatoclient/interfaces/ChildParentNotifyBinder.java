package com.aman.castleglobalzomatoclient.interfaces;

/**
 * Created by Aman on 29-11-2017.
 */

public interface ChildParentNotifyBinder {
    void requestParentNotify();
}
