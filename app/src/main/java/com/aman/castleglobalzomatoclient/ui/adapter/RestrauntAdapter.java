package com.aman.castleglobalzomatoclient.ui.adapter;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.aman.castleglobalzomatoclient.R;
import com.aman.castleglobalzomatoclient.interfaces.ChildParentNotifyBinder;
import com.aman.castleglobalzomatoclient.model.SearchModel;
import com.aman.castleglobalzomatoclient.ui.activites.RestaurantActivity;
import com.aman.castleglobalzomatoclient.utils.PreferencesCache;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Aman on 27-11-2017.
 */

public class RestrauntAdapter extends RecyclerView.Adapter<RestrauntAdapter.ViewHolderImpl> {
    private ArrayList<SearchModel.RestaurantsBean> data = new ArrayList<>();
    private ChildParentNotifyBinder childParentNotifyBinder;

    public RestrauntAdapter(ChildParentNotifyBinder childParentNotifyBinder) {
        this.childParentNotifyBinder = childParentNotifyBinder;
    }

    @Override
    public int getItemViewType(int position) {
        // return position == data.size() ? 1 : 0;
        return 0;
    }

    public void setData(List<SearchModel.RestaurantsBean> data) {
        this.data.clear();
        this.data.addAll(data);
        notifyDataSetChanged();
    }

    @Override
    public ViewHolderImpl onCreateViewHolder(ViewGroup parent, int viewType) {
        return viewType == 1 ? new ProgressViewHolder(new ProgressBar(parent.getContext())) :
                new SearchViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.restraunt_item, parent, false));
    }

    @Override
    public void onBindViewHolder(ViewHolderImpl holder, int position) {
        holder.setData(position);
    }

    @Override
    public int getItemCount() {
        // return data.size() > 0 ? data.size() + 1 : 0;
        return data == null ? 0 : data.size();
    }

    abstract class ViewHolderImpl extends RecyclerView.ViewHolder {

        private ViewHolderImpl(View itemView) {
            super(itemView);
        }

        abstract void setData(int position);
    }

    private class SearchViewHolder extends ViewHolderImpl implements View.OnClickListener {
        private TextView name;
        private ImageView thumb, fav;

        private SearchViewHolder(View itemView) {
            super(itemView);
            itemView.setOnClickListener(this);
            thumb = itemView.findViewById(R.id.rest_image);
            name = itemView.findViewById(R.id.rest_name);
            fav = itemView.findViewById(R.id.rest_fav);
            fav.setOnClickListener(this);
        }

        @Override
        void setData(int position) {
            SearchModel.RestaurantsBean.RestaurantBean restaurantBean = data.get(position).getRestaurant();
            if (restaurantBean.getThumb().trim().length() > 0)
                Picasso.with(itemView.getContext()).load(restaurantBean.getThumb()).placeholder(R.color.cardview_dark_background).into(thumb);
            name.setText(restaurantBean.getName());
            fav.setImageResource(PreferencesCache.isRestaurentSaved(restaurantBean.getId()) ? R.drawable.ic_favorite : R.drawable.ic_favorite_border);
            fav.setTag(PreferencesCache.isRestaurentSaved(restaurantBean.getId()) ? R.drawable.ic_favorite : R.drawable.ic_favorite_border);
            // info.setText(getInfoText(restaurantBean));
        }

        @NonNull
        private String getInfoText(SearchModel.RestaurantsBean.RestaurantBean restaurantBean) {
            StringBuilder stringBuilder = new StringBuilder();
            stringBuilder.append("Serves ");
            stringBuilder.append(restaurantBean.getCuisines());
            stringBuilder.append("\n");
            stringBuilder.append(restaurantBean.getUser_rating().getAggregate_rating());
            for (int i = 0; i < Math.round(Float.valueOf(restaurantBean.getUser_rating().getAggregate_rating())); i++)
                stringBuilder.append("★");
            stringBuilder.append("\n");
            stringBuilder.append("Cost Around ");
            stringBuilder.append(restaurantBean.getAverage_cost_for_two());
            stringBuilder.append(" For 2 Person");
            return stringBuilder.toString();
        }

        @Override
        public void onClick(View view) {
            if (view.equals(fav)) {
                SearchModel.RestaurantsBean.RestaurantBean restaurantBean = data.get(getLayoutPosition()).getRestaurant();
                if (PreferencesCache.isRestaurentSaved(restaurantBean.getId()))
                    PreferencesCache.deleteRestaurent(restaurantBean.getId());
                else
                    PreferencesCache.saveResturent(restaurantBean);
                notifyDataSetChanged();
                childParentNotifyBinder.requestParentNotify();
            } else
                RestaurantActivity.startActivityWith(view.getContext(), data.get(getLayoutPosition()).getRestaurant());
        }
    }

    private class ProgressViewHolder extends ViewHolderImpl {

        public ProgressViewHolder(View itemView) {
            super(itemView);
            itemView.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));
        }

        @Override
        void setData(int position) {

        }
    }
}