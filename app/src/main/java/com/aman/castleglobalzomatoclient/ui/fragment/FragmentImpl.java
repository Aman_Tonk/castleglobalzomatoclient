package com.aman.castleglobalzomatoclient.ui.fragment;

import android.support.v4.app.Fragment;

import com.aman.castleglobalzomatoclient.utils.CommonUtils;

/**
 * Created by Aman on 29-11-2017.
 */

public abstract class FragmentImpl extends Fragment{

    public abstract void delegateSearchResult(String currentKeyword);

    protected boolean isOnline(){
        return CommonUtils.isNetworkAvailable(getContext());
    }
}
