package com.aman.castleglobalzomatoclient.ui.fragment;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.aman.castleglobalzomatoclient.R;
import com.aman.castleglobalzomatoclient.interfaces.ChildParentNotifyBinder;
import com.aman.castleglobalzomatoclient.ui.adapter.SavedRestaurantAdapter;
import com.aman.castleglobalzomatoclient.utils.PreferencesCache;

/**
 * Created by Aman on 29-11-2017.
 */

public class FavFragment extends FragmentImpl implements ChildParentNotifyBinder{
    private View rootView;
    private RecyclerView recyclerView;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.frag_fav, container, false);
        findViews();
        return rootView;
    }

    private void findViews() {
        recyclerView = rootView.findViewById(R.id.fav_recycler);
        recyclerView.setLayoutManager(new GridLayoutManager(getContext(), 3));
        recyclerView.setAdapter(new SavedRestaurantAdapter(this));
        refreshData();
    }

    @Override
    public void onResume() {
        super.onResume();
        refreshData();
    }
    private void refreshData(){
        if(recyclerView == null)
            return;
        if(PreferencesCache.getAllSavedRestaurent().isEmpty()) {
            rootView.findViewById(R.id.helpText).setVisibility(View.VISIBLE);
            recyclerView.setVisibility(View.GONE);
        }else{
            rootView.findViewById(R.id.helpText).setVisibility(View.GONE);
            recyclerView.setVisibility(View.VISIBLE);
        }
        if(recyclerView.getAdapter() != null) {
            ((SavedRestaurantAdapter)recyclerView.getAdapter()).refrestData();
            recyclerView.getAdapter().notifyDataSetChanged();
        }
    }

    @Override
    public void delegateSearchResult(String currentKeyword) {
        Toast.makeText(getContext(),"Office Search Not Implemented",Toast.LENGTH_LONG).show();
    }

    @Override
    public void requestParentNotify() {
        refreshData();
    }
}
