package com.aman.castleglobalzomatoclient.ui.adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.aman.castleglobalzomatoclient.ui.fragment.AboutFragment;
import com.aman.castleglobalzomatoclient.ui.fragment.FavFragment;
import com.aman.castleglobalzomatoclient.ui.fragment.FragmentImpl;
import com.aman.castleglobalzomatoclient.ui.fragment.HomeFragment;

/**
 * Created by Aman on 30-11-2017.
 */

public class PagerAdapter extends FragmentPagerAdapter {
    FragmentImpl homeFragment, favFragment,aboutFragment;
    public PagerAdapter(FragmentManager fm) {
        super(fm);
        homeFragment = new HomeFragment();
        favFragment = new FavFragment();
        aboutFragment = new AboutFragment();
    }

    @Override
    public Fragment getItem(int position) {
        switch (position){
            case 0:
                return homeFragment;
            case 1:
                return favFragment;
            case 2:
                return aboutFragment;
        }
        return null;
    }

    @Override
    public int getCount() {
        return 3;
    }
}
