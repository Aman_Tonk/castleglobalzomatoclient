package com.aman.castleglobalzomatoclient.ui.fragment;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.aman.castleglobalzomatoclient.R;

/**
 * Created by Aman on 29-11-2017.
 */

public class AboutFragment extends FragmentImpl {
    @Override
    public void delegateSearchResult(String currentKeyword) {

    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.frag_about, container, false);
    }
}
