package com.aman.castleglobalzomatoclient.ui.activites;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.aman.castleglobalzomatoclient.R;
import com.aman.castleglobalzomatoclient.model.SearchModel;
import com.aman.castleglobalzomatoclient.utils.PreferencesCache;
import com.google.gson.Gson;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

public class RestaurantActivity extends AppCompatActivity {
    private static SearchModel.RestaurantsBean.RestaurantBean restaurantsBean;

    public static void startActivityWith(Context context, SearchModel.RestaurantsBean.RestaurantBean restaurantsBean) {
        RestaurantActivity.restaurantsBean = restaurantsBean;
        context.startActivity(new Intent(context, RestaurantActivity.class));
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (restaurantsBean == null)
            finish();
        setContentView(R.layout.activity_restaurant);
        if (!restaurantsBean.getThumb().isEmpty())
            Picasso.with(this).load(restaurantsBean.getThumb()).placeholder(R.color.white).into((ImageView) findViewById(R.id.rest_title));
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle(restaurantsBean.getName());
        final FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setImageResource(PreferencesCache.isRestaurentSaved(restaurantsBean.getId()) ? R.drawable.ic_favorite : R.drawable.ic_favorite_border);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String msg;
                if (PreferencesCache.isRestaurentSaved(restaurantsBean.getId())) {
                    PreferencesCache.deleteRestaurent(restaurantsBean.getId());
                    msg = "Restaurant has been deleted!";
                } else {
                    PreferencesCache.saveResturent(restaurantsBean);
                    msg = "Restaurant has been saved!";
                }
                Snackbar.make(view, msg, Snackbar.LENGTH_LONG).show();
                fab.setImageResource(PreferencesCache.isRestaurentSaved(restaurantsBean.getId()) ? R.drawable.ic_favorite : R.drawable.ic_favorite_border);
            }
        });
        try {
            ((TextView) findViewById(R.id.json)).setText(new JSONObject(new Gson().toJson(restaurantsBean)).toString(4));
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        restaurantsBean = null;
    }
}
