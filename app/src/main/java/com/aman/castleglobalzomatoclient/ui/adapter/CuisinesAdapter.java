package com.aman.castleglobalzomatoclient.ui.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.aman.castleglobalzomatoclient.R;
import com.aman.castleglobalzomatoclient.interfaces.ChildParentNotifyBinder;
import com.aman.castleglobalzomatoclient.model.SearchModel;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;

/**
 * Created by Aman on 29-11-2017.
 */

public class CuisinesAdapter extends RecyclerView.Adapter<CuisinesAdapter.ViewHolder> {
    private List<String> names;
    private List<List<SearchModel.RestaurantsBean>> data;

    public CuisinesAdapter(List<String> names, List<List<SearchModel.RestaurantsBean>> data) {
        this.names = names;
        this.data = data;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.cuisine_item, parent, false));
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.cuisineName.setText(names.get(position));
        holder.restrauntAdapter.setData(data.get(position));

    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder implements ChildParentNotifyBinder{
        private TextView cuisineName;
        private RecyclerView recyclerView;
        private RestrauntAdapter restrauntAdapter = new RestrauntAdapter(this);

        private ViewHolder(View itemView) {
            super(itemView);
            cuisineName = itemView.findViewById(R.id.cuisineTitle);
            recyclerView = itemView.findViewById(R.id.rest_recycler);
            recyclerView.setAdapter(restrauntAdapter);
        }

        @Override
        public void requestParentNotify() {
            notifyDataSetChanged();
        }
    }
}
