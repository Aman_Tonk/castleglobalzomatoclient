package com.aman.castleglobalzomatoclient.ui.fragment;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.aman.castleglobalzomatoclient.R;
import com.aman.castleglobalzomatoclient.api.Zomato;
import com.aman.castleglobalzomatoclient.model.SearchModel;
import com.aman.castleglobalzomatoclient.ui.adapter.CuisinesAdapter;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Aman on 29-11-2017.
 */

public class HomeFragment extends FragmentImpl implements Callback<SearchModel> {
    private View rootView;
    private View progressbar;
    private RecyclerView recyclerView;
    private TextView helpText;
    private static List<String> cuisines = new ArrayList<>();
    private static List<List<SearchModel.RestaurantsBean>> data = new ArrayList<>();

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_home, container, false);
        findViews();
        return rootView;
    }

    private void findViews() {
        progressbar = rootView.findViewById(R.id.progressbar);
        helpText = rootView.findViewById(R.id.helpText);
        recyclerView = rootView.findViewById(R.id.rest_recycler);
        recyclerView.setAdapter(new CuisinesAdapter(cuisines, data));
    }

    private void filterData(SearchModel searchModel) {
        Set<String> tmpCuisine = new HashSet<>();
        for (SearchModel.RestaurantsBean restaurantsBean : searchModel.getRestaurants()) {
            tmpCuisine.addAll(Arrays.asList(restaurantsBean.getRestaurant().getCuisines().split(",")));
        }
        cuisines.addAll(tmpCuisine);
        for (String cuisine : tmpCuisine) {
            List<SearchModel.RestaurantsBean> tmpData = new ArrayList<>();
            for (SearchModel.RestaurantsBean restaurantsBean : searchModel.getRestaurants()) {
                if (restaurantsBean.getRestaurant().getCuisines().contains(cuisine))
                    tmpData.add(restaurantsBean);
            }
            data.add(tmpData);
        }
    }

    private boolean isInitialSearch() {
        return data.size() == 0;
    }

    @Override
    public void delegateSearchResult(String currentKeyword) {
        if (isOnline()) {
            helpText.setVisibility(View.GONE);
            data.clear();
            cuisines.clear();
            progressbar.setVisibility(View.VISIBLE);
            Zomato.getInstance().getZomatoAPI().search(Zomato.ZOMATO_KEY, currentKeyword, data.size()).enqueue(this);
        } else {
            Toast.makeText(getContext(), "Please Check your Internet Connection and Try Again", Toast.LENGTH_LONG).show();
        }
    }

    @Override
    public void onResponse(Call<SearchModel> call, Response<SearchModel> response) {
        if (response.body() == null || response.body().getRestaurants() == null)
            return;
        filterData(response.body());
        recyclerView.getAdapter().notifyDataSetChanged();
        helpText.setVisibility(View.GONE);
        progressbar.setVisibility(View.GONE);
        recyclerView.setVisibility(View.VISIBLE);
    }

    @Override
    public void onResume() {
        super.onResume();
        recyclerView.getAdapter().notifyDataSetChanged();
    }

    @Override
    public void onFailure(Call<SearchModel> call, Throwable t) {
        if (isInitialSearch()) {
            helpText.setVisibility(View.VISIBLE);
            helpText.setText("Oops! Something went wrong.");
        } else
            Log.e("Aman", "Oops! Something went wrong.");
        progressbar.setVisibility(View.GONE);
        recyclerView.setVisibility(View.GONE);
    }
}
