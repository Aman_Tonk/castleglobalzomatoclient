package com.aman.castleglobalzomatoclient.ui.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.aman.castleglobalzomatoclient.R;
import com.aman.castleglobalzomatoclient.interfaces.ChildParentNotifyBinder;
import com.aman.castleglobalzomatoclient.model.SearchModel;
import com.aman.castleglobalzomatoclient.ui.activites.RestaurantActivity;
import com.aman.castleglobalzomatoclient.utils.PreferencesCache;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Aman on 29-11-2017.
 */

public class SavedRestaurantAdapter extends RecyclerView.Adapter<SavedRestaurantAdapter.RestViewHolder> {

    private List<SearchModel.RestaurantsBean.RestaurantBean> data = new ArrayList<>();
    private ChildParentNotifyBinder childParentNotifyBinder;

    public SavedRestaurantAdapter(ChildParentNotifyBinder childParentNotifyBinder) {
        this.childParentNotifyBinder = childParentNotifyBinder;
    }

    public void refrestData(){
        data.clear();
        data.addAll(PreferencesCache.getAllSavedRestaurent());
    }
    @Override
    public RestViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new RestViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.restraunt_item, parent, false));
    }

    @Override
    public void onBindViewHolder(RestViewHolder holder, int position) {
        holder.setData(position);
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

     class RestViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        private TextView name;
        private ImageView thumb, fav;

        private RestViewHolder(View itemView) {
            super(itemView);
            itemView.setOnClickListener(this);
            thumb = itemView.findViewById(R.id.rest_image);
            name = itemView.findViewById(R.id.rest_name);
            fav = itemView.findViewById(R.id.rest_fav);
            fav.setOnClickListener(this);
        }

        void setData(int position) {
            SearchModel.RestaurantsBean.RestaurantBean restaurantBean = data.get(position);
            if (restaurantBean.getThumb().trim().length() > 0)
                Picasso.with(itemView.getContext()).load(restaurantBean.getThumb()).placeholder(R.color.cardview_dark_background).into(thumb);
            name.setText(restaurantBean.getName());
            fav.setImageResource(PreferencesCache.isRestaurentSaved(restaurantBean.getId()) ? R.drawable.ic_favorite : R.drawable.ic_favorite_border);
            fav.setTag(PreferencesCache.isRestaurentSaved(restaurantBean.getId()) ? R.drawable.ic_favorite : R.drawable.ic_favorite_border);
        }

        @Override
        public void onClick(View view) {
            if (view.equals(fav)) {
                PreferencesCache.deleteRestaurent(data.get(getLayoutPosition()).getId());
                childParentNotifyBinder.requestParentNotify();
            } else
                RestaurantActivity.startActivityWith(view.getContext(), data.get(getLayoutPosition()));
        }
    }
}
