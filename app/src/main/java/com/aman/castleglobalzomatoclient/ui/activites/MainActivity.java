package com.aman.castleglobalzomatoclient.ui.activites;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.view.KeyEvent;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.aman.castleglobalzomatoclient.R;
import com.aman.castleglobalzomatoclient.interfaces.OnLoadMoreCallback;
import com.aman.castleglobalzomatoclient.ui.adapter.PagerAdapter;
import com.aman.castleglobalzomatoclient.ui.fragment.FragmentImpl;

public class MainActivity extends AppCompatActivity implements TextView.OnEditorActionListener, OnLoadMoreCallback, BottomNavigationView.OnNavigationItemSelectedListener, ViewPager.OnPageChangeListener {
    private ViewPager viewPager;
    private BottomNavigationView bottomNavigationView;
    private PagerAdapter pagerAdapter;
    private String currentKeyword = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        viewPager = findViewById(R.id.pager);
        pagerAdapter = new PagerAdapter(getSupportFragmentManager());
        viewPager.setAdapter(pagerAdapter);
        viewPager.setOffscreenPageLimit(4);
        viewPager.addOnPageChangeListener(this);
        bottomNavigationView = findViewById(R.id.bottom_navigation);
        bottomNavigationView.setOnNavigationItemSelectedListener(this);
        bottomNavigationView.setSelectedItemId(bottomNavigationView.getMenu().getItem(0).getItemId());
        ((EditText) findViewById(R.id.searchEdittext)).setOnEditorActionListener(this);
    }

    private void performSearch() {
        hideKeyboard();
        ((FragmentImpl) pagerAdapter.getItem(viewPager.getCurrentItem())).delegateSearchResult(currentKeyword);
    }

    @Override
    public boolean onEditorAction(TextView textView, int actionId, KeyEvent keyEvent) {
        currentKeyword = textView.getText().toString();
        if (actionId == EditorInfo.IME_ACTION_SEARCH && !currentKeyword.isEmpty()) {
            performSearch();
            return true;
        }
        return false;
    }

    public void hideKeyboard() {
        View view = this.getCurrentFocus();
        if (view != null) {
            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            if (imm != null) imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
    }

    @Override
    public void onLoadMore() {
        //performSearch();
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        int moveTo = -1;
        switch (item.getItemId()) {
            case R.id.action_home:
                moveTo = 0;
                break;
            case R.id.action_fav:
                moveTo = 1;
                break;
            case R.id.action_about:
                moveTo = 2;
                break;
        }
        if (moveTo != -1) {
            viewPager.setCurrentItem(moveTo);
            return true;
        }
        return false;
    }

    private long backPressMillis;

    @Override
    public void onBackPressed() {
        if (backPressMillis + 2000 > System.currentTimeMillis())
            super.onBackPressed();
        else {
            Toast.makeText(getApplicationContext(), "Tap Once More to Exit", Toast.LENGTH_SHORT).show();
            backPressMillis = System.currentTimeMillis();
        }
    }

    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

    }

    @Override
    public void onPageSelected(int position) {
        pagerAdapter.getItem(position).onResume();
        bottomNavigationView.setSelectedItemId(bottomNavigationView.getMenu().getItem(position).getItemId());
    }

    @Override
    public void onPageScrollStateChanged(int state) {

    }
}
