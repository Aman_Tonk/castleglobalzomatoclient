package com.aman.castleglobalzomatoclient.utils;

import android.content.Context;
import android.content.SharedPreferences;

import com.aman.castleglobalzomatoclient.model.SearchModel;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Aman on 28-11-2017.
 */

public class PreferencesCache {
    private static SharedPreferences sharedPreferences;

    public static void init(Context context) {
        sharedPreferences = context.getSharedPreferences("fav_rest", Context.MODE_PRIVATE);
    }

    private static boolean has(String key) {
        return sharedPreferences.contains(key);
    }

    private static void delete(String key){
        sharedPreferences.edit().remove(key).apply();
    }

    private static void save(String key, String value) {
        sharedPreferences.edit().putString(key, value).apply();
    }

    public static void saveResturent(SearchModel.RestaurantsBean.RestaurantBean restaurantBean) {
        save(restaurantBean.getId(), restaurantBean.toString());
    }

    public static List<SearchModel.RestaurantsBean.RestaurantBean> getAllSavedRestaurent(){
        List<SearchModel.RestaurantsBean.RestaurantBean> result = new ArrayList<>();
        Gson gson = new Gson();
        for (Object o : sharedPreferences.getAll().values())
            result.add(gson.fromJson(o.toString(), SearchModel.RestaurantsBean.RestaurantBean.class));
        return result;
    }
    public static boolean isRestaurentSaved(String rest_id){
        return has(rest_id);
    }

    public static void deleteRestaurent(String rest_id){
        delete(rest_id);
    }
}
