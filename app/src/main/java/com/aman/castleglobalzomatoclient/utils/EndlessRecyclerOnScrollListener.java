package com.aman.castleglobalzomatoclient.utils;

import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.aman.castleglobalzomatoclient.interfaces.OnLoadMoreCallback;

public class EndlessRecyclerOnScrollListener extends RecyclerView.OnScrollListener {
    private int mPreviousTotal = 0;
    private boolean mLoading = true;
    private OnLoadMoreCallback onLoadMoreCallback;

    public EndlessRecyclerOnScrollListener(OnLoadMoreCallback onLoadMoreCallback) {
        this.onLoadMoreCallback = onLoadMoreCallback;
    }

    @Override
    public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
        super.onScrolled(recyclerView, dx, dy);
        int totalItemCount = recyclerView.getLayoutManager().getItemCount();
        if (mLoading) {
            if (totalItemCount > mPreviousTotal) {
                mLoading = false;
                mPreviousTotal = totalItemCount;
            }
        }
        int visibleThreshold = 5;
        if (!mLoading && (totalItemCount - recyclerView.getChildCount()) <= (((LinearLayoutManager) recyclerView.getLayoutManager()).findFirstVisibleItemPosition() + visibleThreshold)) {
            onLoadMoreCallback.onLoadMore();
            mLoading = true;
        }
    }

}