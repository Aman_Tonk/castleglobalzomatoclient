package com.aman.castleglobalzomatoclient;

import android.app.Application;

import com.aman.castleglobalzomatoclient.utils.PreferencesCache;
import com.squareup.picasso.OkHttpDownloader;
import com.squareup.picasso.Picasso;

/**
 * Created by Aman on 27-11-2017.
 */

public class CastleGlobalZomato extends Application {
    @Override
    public void onCreate() {
        super.onCreate();
        PreferencesCache.init(this);
//        Picasso.setSingletonInstance(new Picasso.Builder(this).downloader(new OkHttpDownloader(this, 1024 * 1024)).build());
    }
}
