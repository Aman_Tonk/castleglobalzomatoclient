package com.aman.castleglobalzomatoclient.model;

import com.google.gson.Gson;

import java.util.List;

/**
 * Created by Aman on 27-11-2017.
 */

public class CuisineModel {
    private List<CuisinesBean> cuisines;

    @Override
    public String toString() {
        return new Gson().toJson(this);
    }

    public List<CuisinesBean> getCuisines() {
        return cuisines;
    }

    public void setCuisines(List<CuisinesBean> cuisines) {
        this.cuisines = cuisines;
    }

    public static class CuisinesBean {
        private CuisineBean cuisine;

        public CuisineBean getCuisine() {
            return cuisine;
        }

        public void setCuisine(CuisineBean cuisine) {
            this.cuisine = cuisine;
        }

        public static class CuisineBean {
            private int cuisine_id;
            private String cuisine_name;

            public int getCuisine_id() {
                return cuisine_id;
            }

            public void setCuisine_id(int cuisine_id) {
                this.cuisine_id = cuisine_id;
            }

            public String getCuisine_name() {
                return cuisine_name;
            }

            public void setCuisine_name(String cuisine_name) {
                this.cuisine_name = cuisine_name;
            }
        }
    }
}
