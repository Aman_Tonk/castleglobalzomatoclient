package com.aman.castleglobalzomatoclient.api;

import com.aman.castleglobalzomatoclient.model.CuisineModel;
import com.aman.castleglobalzomatoclient.model.SearchModel;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Query;

/**
 * Created by Aman on 27-11-2017.
 */

public interface ZomatoAPI {
    @GET("search")
    Call<SearchModel> search(@Header("user-key") String key, @Query("q") String keyword, @Query("start") int offset);

    @GET("cuisines")
    Call<CuisineModel> getCuisineList(@Header("user-key") String key, @Query("city_id") int cityCode);
}
