package com.aman.castleglobalzomatoclient.api;

import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by Aman on 27-11-2017.
 */

public class Zomato {
    public static final String ZOMATO_KEY = "fb5c1fbf81f59946780bd7796e875df5";
    private ZomatoAPI zomatoAPI;
    private static final Zomato ourInstance = new Zomato();

    public static Zomato getInstance() {
        return ourInstance;
    }

    private Zomato() {
        zomatoAPI = new Retrofit.Builder().addConverterFactory(GsonConverterFactory.create()).baseUrl("https://developers.zomato.com/api/v2.1/").build().create(ZomatoAPI.class);
    }

    public ZomatoAPI getZomatoAPI() {
        return zomatoAPI;
    }
}
